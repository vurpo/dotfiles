execute pathogen#infect()

if has('gui_running')
  set guifont=Roboto\ Mono\ 11
endif
colorscheme slate

set nocompatible              " be iMproved, required
filetype off                  " required

autocmd BufRead,BufNewFile Cargo.toml,Cargo.lock,*.rs compiler cargo

filetype plugin indent on    " required

nmap <F8> :TagbarToggle<CR>
"autocmd VimEnter * nested :TagbarOpen
nmap <F12> :AutoSaveToggle<CR>
set mouse=a
set expandtab
set shiftwidth=4
set softtabstop=4
set cursorline
set number
set relativenumber
set clipboard^=unnamed,unnamedplus
let g:rust_recommended_style = 0
set hidden
set completeopt+=longest
let g:racer_cmd = "/home/max/.cargo/bin/racer"
let $RUST_SRC_PATH = "/home/max/.multirust/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src/"
let g:racer_experimental_completer = 1
let g:deoplete#enable_at_startup = 1
let g:SuperTabDefaultCompletionType = "<c-n>"
let g:SuperTabContextDefaultCompletionType = "<c-n>"
set updatetime=200
"au CursorHold * silent! update

let g:ctrlp_custom_ignore = {
	\ 'dir':  '\v[\/]target$',
	\ }

let g:tagbar_type_rust = {
   \ 'ctagstype' : 'rust',
   \ 'kinds' : [
       \'T:types,type definitions',
       \'f:functions,function definitions',
       \'g:enum,enumeration names',
       \'s:structure names',
       \'m:modules,module names',
       \'c:consts,static constants',
       \'t:traits,traits',
       \'i:impls,trait implementations',
   \]
   \}

set statusline+=%#warningmsg#
set statusline+=%*

let g:auto_save = 1
let g:auto_save_in_insert_mode = 0

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
